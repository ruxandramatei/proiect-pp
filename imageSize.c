unsigned int imageSize(unsigned char *header)
{
    return *(header + 2) + (unsigned int)*(header + 3) * 256 + (unsigned int)*(header + 4) * 256 * 256 + (unsigned int)*(header + 5) * 256 * 256 * 256;
}