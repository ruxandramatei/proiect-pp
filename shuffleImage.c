unsigned char *shuffleImage(unsigned char *image, unsigned int *shuffle, unsigned int size)
{
    unsigned int imageCounter, shuffleCounter;
    unsigned char *shuffledImage;

    shuffledImage = (unsigned char *)malloc(sizeof(unsigned char) * size);

    for (imageCounter = 0, shuffleCounter = 0; imageCounter < size; imageCounter += 3, shuffleCounter++)
    {
        unsigned int temp = *(shuffle + shuffleCounter) * 3;
        *(shuffledImage + temp) = *(image + imageCounter);
        *(shuffledImage + temp + 1) = *(image + imageCounter + 1);
        *(shuffledImage + temp + 2) = *(image + imageCounter + 2);
    }
    free(image);
    return shuffledImage;
}