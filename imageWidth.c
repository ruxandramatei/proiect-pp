unsigned int imageWidth(unsigned char *header)
{
    return *(header + 18) + (unsigned int)*(header + 19) * 256 + (unsigned int)*(header + 20) * 256 * 256 + (unsigned int)*(header + 21) * 256 * 256 * 256;
}