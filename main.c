#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BYTE 256
#define HEADER_SIZE 54
#define PATTERN_WIDTH 11
#define PATTERN_WIDTH_WITH_PADDING 12
#define PATTERN_HEIGHT 15
#define PATTERN_SIZE 165
#define DETECTION_REUNION_ELEMENT_SIZE 32

#include "xorshift.c"
#include "imageSize.c"
#include "imageWidth.c"
#include "imageHeight.c"
#include "shuffle.c"
#include "shuffleImage.c"
#include "shuffleInvert.c"
#include "imageToGrayscale.c"

typedef struct point
{
    int x, y;
    int digit;
    long double correlation;
} WINDOWS;

void readBMPImage(char *file, unsigned char **header, unsigned char **image)
{
    unsigned char *imgData;
    unsigned char *img;
    unsigned int width, height, size, givenImageSize;

    FILE *in = fopen(file, "rb");

    if (in == NULL)
    {
        perror("Error at reading BMP file");
        printf("Filename : %s\n", file);
        exit(EXIT_FAILURE);
    }

    fseek(in, 0, SEEK_END);
    givenImageSize = ftell(in);
    rewind(in);

    *header = (unsigned char *)malloc(HEADER_SIZE);
    imgData = (unsigned char *)malloc((givenImageSize + 1) - HEADER_SIZE);
    img = (unsigned char *)malloc((givenImageSize + 1) - HEADER_SIZE);

    fread(*header, HEADER_SIZE, 1, in);
    fread(imgData, givenImageSize - HEADER_SIZE, 1, in);

    width = imageWidth(*header);
    height = imageHeight(*header);

    unsigned int i, j, temp;

    for (i = 0, temp = height - 1; i < height; i++, temp--)
    {
        for (j = 0; j < width; j++)
        {
            *(img + temp * width * 3 + j * 3) = *(imgData + i * width * 3 + j * 3);
            *(img + temp * width * 3 + j * 3 + 1) = *(imgData + i * width * 3 + j * 3 + 1);
            *(img + temp * width * 3 + j * 3 + 2) = *(imgData + i * width * 3 + j * 3 + 2);
        }
    }
    *image = img;
    fclose(in);
}

void saveBMPImage(char *file, unsigned char *imageHeader, unsigned char *imageToDisplay)
{
    unsigned char *img;
    unsigned int width, height, givenImageSize = imageSize(imageHeader);

    width = imageWidth(imageHeader);
    height = imageHeight(imageHeader);

    img = (unsigned char *)malloc((givenImageSize + 1) - HEADER_SIZE);

    unsigned int i, j, temp;

    for (i = 0, temp = height - 1; i < height; i++, temp--)
    {
        for (j = 0; j < width; j++)
        {
            *(img + temp * width * 3 + j * 3) = *(imageToDisplay + i * width * 3 + j * 3);
            *(img + temp * width * 3 + j * 3 + 1) = *(imageToDisplay + i * width * 3 + j * 3 + 1);
            *(img + temp * width * 3 + j * 3 + 2) = *(imageToDisplay + i * width * 3 + j * 3 + 2);
        }
    }

    FILE *out = fopen(file, "wb");

    if (out == NULL)
    {
        perror("Error at saving BMP file");
        exit(EXIT_FAILURE);
    }

    fwrite(imageHeader, HEADER_SIZE, 1, out);
    fwrite(img, givenImageSize - HEADER_SIZE, 1, out);

    fclose(out);
}

void encryption(char *sourceFile, char *destinationFile, char *secretKeyFile)
{
    unsigned char *image, *header;
    unsigned int width, height, size;

    readBMPImage(sourceFile, &header, &image);

    width = imageWidth(header);
    height = imageHeight(header);
    size = imageSize(header);

    FILE *secretValues = fopen(secretKeyFile, "r");

    if (secretValues == NULL)
    {
        perror("Error at opening secret key file (encryption)");
        printf("Filename : %s\n", secretKeyFile);
        exit(EXIT_FAILURE);
    }

    unsigned int seed, secretValue;
    fscanf(secretValues, "%u", &seed);
    fscanf(secretValues, "%u", &secretValue);
    fclose(secretValues);

    unsigned int *randomNumbers;
    unsigned int randomNumbersSize = 2 * width * height;

    randomNumbers = XORSHIFT32(randomNumbersSize, seed);

    unsigned int *shuffled;
    shuffled = shuffleOperation(randomNumbers, width * height);
    image = shuffleImage(image, shuffled, size - HEADER_SIZE);
    //pixelii ii permuti cate 3 B G R (pt ca little endian)
    //int x = (number >> (8*n)) & 0xff;
    //temp = (x3, x2, x1, x0) cu cel mai semnificativ octet x3
    unsigned int temp = secretValue ^ (*(randomNumbers + width * height));
    unsigned int x0 = (temp >> (8 * 0)) & 0xff;
    unsigned int x1 = (temp >> (8 * 1)) & 0xff;
    unsigned int x2 = (temp >> (8 * 2)) & 0xff;
    //Pb ^ x0, Pg ^ x1, Pr ^ x2
    *(image) = x0 ^ (*(image));
    *(image + 1) = x1 ^ (*(image + 1));
    *(image + 2) = x2 ^ (*(image + 2));

    unsigned int imageCounter, randomCounter;
    unsigned int imageSizeWithoutHeader = size - HEADER_SIZE;

    for (imageCounter = 3, randomCounter = width * height + 1; imageCounter < imageSizeWithoutHeader; imageCounter += 3, randomCounter++)
    {
        *(image + imageCounter) = (*(image + imageCounter)) ^ (*(image + imageCounter - 3));
        *(image + imageCounter + 1) = (*(image + imageCounter + 1)) ^ (*(image + imageCounter - 2));
        *(image + imageCounter + 2) = (*(image + imageCounter + 2)) ^ (*(image + imageCounter - 1));

        x0 = ((*(randomNumbers + randomCounter)) >> (8 * 0)) & 0xff;
        x1 = ((*(randomNumbers + randomCounter)) >> (8 * 1)) & 0xff;
        x2 = ((*(randomNumbers + randomCounter)) >> (8 * 2)) & 0xff;

        *(image + imageCounter) = (*(image + imageCounter)) ^ x0;
        *(image + imageCounter + 1) = (*(image + imageCounter + 1)) ^ x1;
        *(image + imageCounter + 2) = (*(image + imageCounter + 2)) ^ x2;
    }

    saveBMPImage(destinationFile, header, image);
    free(header);
    free(image);
    free(randomNumbers);
    free(shuffled);
}

void decryption(char *sourceFile, char *destinationFile, char *secretKeyFile)
{

    unsigned char *image, *header, *decryptedImage;
    unsigned long long width, height, size;

    readBMPImage(sourceFile, &header, &image);

    width = imageWidth(header);
    height = imageHeight(header);
    size = imageSize(header);

    FILE *secretValues = fopen(secretKeyFile, "r");

    if (secretValues == NULL)
    {
        perror("Error at opening secret key file (decryption)");
        printf("Filename : %s\n", secretKeyFile);
        exit(EXIT_FAILURE);
    }

    unsigned int seed, secretValue;
    fscanf(secretValues, "%u", &seed);
    fscanf(secretValues, "%u", &secretValue);
    fclose(secretValues);

    unsigned int *randomNumbers;
    unsigned int randomNumbersSize = 2 * width * height;

    randomNumbers = XORSHIFT32(randomNumbersSize, seed);

    unsigned int *shuffled;
    shuffled = shuffleOperation(randomNumbers, width * height);
    shuffled = shuffleInvert(shuffled, width * height);

    //pixelii ii permuti cate 3 B G R (pt ca little endian)
    //int x = (number >> (8*n)) & 0xff;
    //temp = (x3, x2, x1, x0) cu cel mai semnificativ octet x3
    unsigned int temp = secretValue ^ (*(randomNumbers + width * height));
    unsigned int x0 = (temp >> (8 * 0)) & 0xff;
    unsigned int x1 = (temp >> (8 * 1)) & 0xff;
    unsigned int x2 = (temp >> (8 * 2)) & 0xff;
    //Pb ^ x0, Pg ^ x1, Pr ^ x2
    decryptedImage = (unsigned char *)malloc(sizeof(unsigned char) * size);
    *(decryptedImage) = x0 ^ (*(image));
    *(decryptedImage + 1) = x1 ^ (*(image + 1));
    *(decryptedImage + 2) = x2 ^ (*(image + 2));

    unsigned int imageCounter, randomCounter;
    unsigned int imageSizeWithoutHeader = size - HEADER_SIZE;

    for (imageCounter = 3, randomCounter = width * height + 1; imageCounter < imageSizeWithoutHeader; imageCounter += 3, randomCounter++)
    {
        *(decryptedImage + imageCounter) = (*(image + imageCounter)) ^ (*(image + imageCounter - 3));
        *(decryptedImage + imageCounter + 1) = (*(image + imageCounter + 1)) ^ (*(image + imageCounter - 2));
        *(decryptedImage + imageCounter + 2) = (*(image + imageCounter + 2)) ^ (*(image + imageCounter - 1));

        x0 = ((*(randomNumbers + randomCounter)) >> (8 * 0)) & 0xff;
        x1 = ((*(randomNumbers + randomCounter)) >> (8 * 1)) & 0xff;
        x2 = ((*(randomNumbers + randomCounter)) >> (8 * 2)) & 0xff;

        *(decryptedImage + imageCounter) = (*(decryptedImage + imageCounter)) ^ x0;
        *(decryptedImage + imageCounter + 1) = (*(decryptedImage + imageCounter + 1)) ^ x1;
        *(decryptedImage + imageCounter + 2) = (*(decryptedImage + imageCounter + 2)) ^ x2;
    }

    decryptedImage = shuffleImage(decryptedImage, shuffled, imageSizeWithoutHeader);

    saveBMPImage(destinationFile, header, decryptedImage);
    free(header);
    free(image);
    free(randomNumbers);
    free(shuffled);
    free(decryptedImage);
};

void chiSqared(char *sourceFile)
{

    unsigned char *image, *header;
    unsigned int width, height, size;

    readBMPImage(sourceFile, &header, &image);

    width = imageWidth(header);
    height = imageHeight(header);
    size = imageSize(header);

    double *frequencyR, *frequencyB, *frequencyG;
    double estimatedFrequency = (double)width * height / 256;
    double chiSqaredValueR, chiSqaredValueB, chiSqaredValueG;
    unsigned int i;

    chiSqaredValueR = chiSqaredValueB = chiSqaredValueG = 0;
    frequencyR = (double *)malloc(sizeof(double) * 256);
    frequencyG = (double *)malloc(sizeof(double) * 256);
    frequencyB = (double *)malloc(sizeof(double) * 256);

    for (i = 0; i <= MAX_BYTE; i++)
    {
        frequencyR[i] = frequencyB[i] = frequencyG[i] = 0;
    }

    for (i = 0; i < size - HEADER_SIZE; i += 3)
    {

        frequencyB[*(image + i)]++;
        frequencyG[*(image + i + 1)]++;
        frequencyR[*(image + i + 2)]++;
    }

    for (i = 0; i < 256; i++)
    {
        chiSqaredValueB = chiSqaredValueB + (frequencyB[i] - estimatedFrequency) *
                                                (frequencyB[i] - estimatedFrequency) / estimatedFrequency;

        chiSqaredValueG = chiSqaredValueG + (frequencyG[i] - estimatedFrequency) *
                                                (frequencyG[i] - estimatedFrequency) / estimatedFrequency;

        chiSqaredValueR = chiSqaredValueR + (frequencyR[i] - estimatedFrequency) *
                                                (frequencyR[i] - estimatedFrequency) / estimatedFrequency;
    }

    printf("Chi-squared test on RGB channels for %s :\n", sourceFile);
    printf("%.2f\n", chiSqaredValueR);
    printf("%.2f\n", chiSqaredValueG);
    printf("%.2f\n", chiSqaredValueB);
    printf("\n");

    free(frequencyB);
    free(frequencyG);
    free(frequencyR);
}

WINDOWS *templateMatching(unsigned char *image, unsigned char *pattern, double limit, int digit, int *temporaryWindowsNumber)
{
    imageToGrayscale(&image);
    imageToGrayscale(&pattern);

    unsigned int imageLineCounter, imageColumnCounter, patternLineCounter, patternColumnCounter;
    unsigned int givenImageWidth, givenImageHeight;

    int windowsCounter = 0;
    WINDOWS *windowsFound;
    windowsFound = (WINDOWS *)malloc(sizeof(struct point));

    givenImageWidth = imageWidth(image);
    if (givenImageWidth % 4 == 1)
    {
        givenImageWidth += 3;
    }
    else if (givenImageWidth % 4 == 2)
    {
        givenImageWidth += 2;
    }
    else if (givenImageWidth % 4 == 3)
    {
        givenImageWidth += 1;
    }
    givenImageHeight = imageHeight(image);

    int i, j;
    int grayscaleIntensity = 0;
    long double standardDerivationPattern = 0, averageGrayscaleIntensity = 0;

    for (i = 0; i < PATTERN_HEIGHT; i++)
    {
        for (j = 0; j < PATTERN_WIDTH; j++)
        {
            int tempValue = *(pattern + HEADER_SIZE + i * PATTERN_WIDTH_WITH_PADDING * 3 + j * 3);
            averageGrayscaleIntensity = averageGrayscaleIntensity + tempValue;
        }
    }
    averageGrayscaleIntensity = averageGrayscaleIntensity / PATTERN_SIZE;

    for (i = 0; i < PATTERN_HEIGHT; i++)
    {
        for (j = 0; j < PATTERN_WIDTH; j++)
        {
            grayscaleIntensity = *(pattern + HEADER_SIZE + i * PATTERN_WIDTH_WITH_PADDING * 3 + j * 3);
            standardDerivationPattern = standardDerivationPattern + (grayscaleIntensity - averageGrayscaleIntensity) *
                                                                        (grayscaleIntensity - averageGrayscaleIntensity);
        }
    }

    standardDerivationPattern = standardDerivationPattern / (PATTERN_SIZE - 1);
    standardDerivationPattern = sqrt(standardDerivationPattern);

    for (imageLineCounter = 0; imageLineCounter <= givenImageHeight - PATTERN_HEIGHT; imageLineCounter++)
    {
        for (imageColumnCounter = 0; imageColumnCounter <= givenImageWidth - PATTERN_WIDTH; imageColumnCounter++)
        {
            long double correlation = 0, standardDerivationImage = 0, averageGrayscaleIntensityImage = 0;
            int grayscaleIntensityImage = 0;
            int lineLimit = imageLineCounter + PATTERN_HEIGHT, columnLimit = imageColumnCounter + PATTERN_WIDTH;

            for (i = imageLineCounter; i < lineLimit; i++)
            {
                for (j = imageColumnCounter; j < columnLimit; j++)
                {
                    int tempValue = *(image + HEADER_SIZE + i * givenImageWidth * 3 + j * 3);
                    averageGrayscaleIntensityImage = averageGrayscaleIntensityImage + tempValue;
                }
            }

            averageGrayscaleIntensityImage = averageGrayscaleIntensityImage / PATTERN_SIZE;

            for (i = imageLineCounter; i < lineLimit; i++)
            {
                for (j = imageColumnCounter; j < columnLimit; j++)
                {
                    grayscaleIntensityImage = *(image + HEADER_SIZE + i * givenImageWidth * 3 + j * 3);
                    standardDerivationImage = standardDerivationImage + (grayscaleIntensityImage -
                                                                         averageGrayscaleIntensityImage) *
                                                                            (grayscaleIntensityImage - averageGrayscaleIntensityImage);
                }
            }

            standardDerivationImage = standardDerivationImage / (PATTERN_SIZE - 1);
            standardDerivationImage = sqrt(standardDerivationImage);

            if (averageGrayscaleIntensityImage == 0 || standardDerivationImage == 0)
            {
                continue;
            }

            long double sum = 0;
            int auxI = 0, auxJ;
            for (i = imageLineCounter; i < lineLimit; i++)
            {
                auxJ = 0;
                for (j = imageColumnCounter; j < columnLimit; j++)
                {
                    grayscaleIntensity = *(pattern + HEADER_SIZE + auxI * PATTERN_WIDTH_WITH_PADDING * 3 + auxJ * 3);
                    grayscaleIntensityImage = *(image + HEADER_SIZE + i * givenImageWidth * 3 + j * 3);

                    sum = sum + (grayscaleIntensityImage - averageGrayscaleIntensityImage) *
                                    (grayscaleIntensity - averageGrayscaleIntensity) /
                                    (standardDerivationImage * standardDerivationPattern);
                    auxJ++;
                }
                auxI++;
            }
            correlation = sum / PATTERN_SIZE;
            if (correlation > limit)
            {
                windowsCounter++;
                windowsFound = (WINDOWS *)realloc(windowsFound, sizeof(struct point) * windowsCounter);
                (windowsFound + windowsCounter - 1)->x = imageLineCounter;
                (windowsFound + windowsCounter - 1)->y = imageColumnCounter;
                (windowsFound + windowsCounter - 1)->digit = digit;
                (windowsFound + windowsCounter - 1)->correlation = correlation;
            }
        }
    }

    *temporaryWindowsNumber = windowsCounter;

    return windowsFound;
}

void conturing(unsigned char **originalImage, WINDOWS *windowPoint, int colorR, int colorG, int colorB)
{
    int imageLineCounter = windowPoint->x;
    int imageColumnCounter = windowPoint->y;
    int givenImageWidth;
    int lineLimit = imageLineCounter + PATTERN_HEIGHT, columnLimit = imageColumnCounter + PATTERN_WIDTH;
    int i, j;

    givenImageWidth = imageWidth(*originalImage);
    if (givenImageWidth % 4 == 1)
    {
        givenImageWidth += 3;
    }
    else if (givenImageWidth % 4 == 2)
    {
        givenImageWidth += 2;
    }
    else if (givenImageWidth % 4 == 3)
    {
        givenImageWidth += 1;
    }

    for (i = imageLineCounter; i < lineLimit; i++)
    {
        *(*originalImage + HEADER_SIZE + i * givenImageWidth * 3 + imageColumnCounter * 3) = colorB;
        *(*originalImage + HEADER_SIZE + i * givenImageWidth * 3 + imageColumnCounter * 3 + 1) = colorG;
        *(*originalImage + HEADER_SIZE + i * givenImageWidth * 3 + imageColumnCounter * 3 + 2) = colorR;

        *(*originalImage + HEADER_SIZE + i * givenImageWidth * 3 + (imageColumnCounter + PATTERN_WIDTH - 1) * 3) = colorB;
        *(*originalImage + HEADER_SIZE + i * givenImageWidth * 3 + (imageColumnCounter + PATTERN_WIDTH - 1) * 3 + 1) = colorG;
        *(*originalImage + HEADER_SIZE + i * givenImageWidth * 3 + (imageColumnCounter + PATTERN_WIDTH - 1) * 3 + 2) = colorR;
    }
    for (j = imageColumnCounter; j < columnLimit; j++)
    {
        *(*originalImage + HEADER_SIZE + imageLineCounter * givenImageWidth * 3 + j * 3) = colorB;
        *(*originalImage + HEADER_SIZE + imageLineCounter * givenImageWidth * 3 + j * 3 + 1) = colorG;
        *(*originalImage + HEADER_SIZE + imageLineCounter * givenImageWidth * 3 + j * 3 + 2) = colorR;

        *(*originalImage + HEADER_SIZE + (lineLimit - 1) * givenImageWidth * 3 + j * 3) = colorB;
        *(*originalImage + HEADER_SIZE + (lineLimit - 1) * givenImageWidth * 3 + j * 3 + 1) = colorG;
        *(*originalImage + HEADER_SIZE + (lineLimit - 1) * givenImageWidth * 3 + j * 3 + 2) = colorR;
    }
}

void conturingFinalDetections(unsigned char **originalImage, WINDOWS *detectionsReunion, int numberOfDetections)
{
    int i = 0;
    for (i = 0; i < numberOfDetections; i++)
    {
        int colorR, colorG, colorB;

        switch ((detectionsReunion + i)->digit)
        {
        case 0:
        {
            colorR = 255;
            colorG = 0;
            colorB = 0;

            break;
        };
        case 1:
        {
            colorR = 255;
            colorG = 255;
            colorB = 0;

            break;
        };
        case 2:
        {
            colorR = 0;
            colorG = 255;
            colorB = 0;

            break;
        };
        case 3:
        {
            colorR = 0;
            colorG = 255;
            colorB = 255;

            break;
        };
        case 4:
        {
            colorR = 255;
            colorG = 0;
            colorB = 255;

            break;
        };
        case 5:
        {
            colorR = 0;
            colorG = 0;
            colorB = 255;

            break;
        };
        case 6:
        {
            colorR = 192;
            colorG = 192;
            colorB = 192;

            break;
        };
        case 7:
        {
            colorR = 255;
            colorG = 140;
            colorB = 0;

            break;
        };
        case 8:
        {
            colorR = 128;
            colorG = 0;
            colorB = 128;

            break;
        }
        case 9:
        {
            colorR = 128;
            colorG = 0;
            colorB = 0;

            break;
        }
        }
        conturing(originalImage, (detectionsReunion + i), colorR, colorG, colorB);
    }
}

int compare(const void *firstElement, const void *secondElement)
{
    long double firstElementCorrelation = ((WINDOWS *)firstElement)->correlation;
    long double secondElementCorrelation = ((WINDOWS *)secondElement)->correlation;

    if (firstElementCorrelation > secondElementCorrelation)
    {
        return -1;
    }
    if (firstElementCorrelation < secondElementCorrelation)
    {
        return 1;
    }
    return 0;
}

void sortDetectionsArray(WINDOWS **detectionsReunion, int numberOfDetections)
{
    qsort(*detectionsReunion, numberOfDetections, DETECTION_REUNION_ELEMENT_SIZE, compare);
}

long int intersection(int x_i, int y_i, int x_j, int y_j)
{
    int intersectionHeight = 0;
    int intersectionWidth = 0;
    if (y_i <= y_j && y_j < y_i + PATTERN_WIDTH)
    {
        if (x_j <= x_i && x_i < x_j + PATTERN_HEIGHT)
        {
            intersectionHeight = (x_j + PATTERN_HEIGHT - x_i);
        }
        else if (x_j >= x_i && x_j < x_i + PATTERN_HEIGHT)
        {
            intersectionHeight = (x_i + PATTERN_HEIGHT - x_j);
        }
        intersectionWidth = y_i + PATTERN_WIDTH - y_j;
    }
    long int area = intersectionHeight * intersectionWidth;
    return area;
}

long int detectionsIntersection(int x_i, int y_i, int x_j, int y_j)
{
    if (y_i <= y_j)
    {
        return intersection(x_i, y_i, x_j, y_j);
    }
    else
    {
        return intersection(x_j, y_j, x_i, y_i);
    }
}

double overlay(int x_i, int y_i, int x_j, int y_j)
{
    long int detectionsArea = PATTERN_HEIGHT * PATTERN_WIDTH;
    long int intersectionOfDetections = detectionsIntersection(x_i, y_i, x_j, y_j);
    if (intersectionOfDetections == 0)
    {
        return 0;
    }
    double detectionsOverlay = (double)intersectionOfDetections / (2 * detectionsArea - intersectionOfDetections);
    return detectionsOverlay;
}

void nonMaximalElimination(WINDOWS **detectionsReunion, unsigned int *numberOfDetections)
{
    sortDetectionsArray(detectionsReunion, *numberOfDetections);
    int i, j;
    unsigned int numberOfFinalDetections = 0;
    for (i = 0; i < *numberOfDetections; i++)
    {
        if ((*detectionsReunion + i)->digit != -1)
        {
            for (j = i + 1; j < *numberOfDetections; j++)
            {
                int x_i = (*detectionsReunion + i)->x;
                int y_i = (*detectionsReunion + i)->y;
                int x_j = (*detectionsReunion + j)->x;
                int y_j = (*detectionsReunion + j)->y;
                if (overlay(x_i, y_i, x_j, y_j) > 0.2)
                {
                    (*detectionsReunion + j)->digit = -1;
                }
            }
            numberOfFinalDetections++;
        }
    }
    WINDOWS *finalDetectionsReunion;
    finalDetectionsReunion = (WINDOWS *)malloc(sizeof(struct point) * numberOfFinalDetections);
    j = 0;
    for (i = 0; i < *numberOfDetections; i++)
    {
        if ((*detectionsReunion + i)->digit != -1)
        {
            (finalDetectionsReunion + j)->x = (*detectionsReunion + i)->x;
            (finalDetectionsReunion + j)->y = (*detectionsReunion + i)->y;
            (finalDetectionsReunion + j)->digit = (*detectionsReunion + i)->digit;
            (finalDetectionsReunion + j)->correlation = (*detectionsReunion + i)->correlation;
            j++;
        }
    }
    free(*detectionsReunion);
    *detectionsReunion = finalDetectionsReunion;
    *numberOfDetections = numberOfFinalDetections;
}

int main()
{

    char *pathEncryptionSource, *pathEncryptionDestination, *pathEncryptionSecretKey;

    pathEncryptionSource = (char *)malloc(sizeof(char) * 31);
    pathEncryptionDestination = (char *)malloc(sizeof(char) * 31);
    pathEncryptionSecretKey = (char *)malloc(sizeof(char) * 31);

    FILE *pathsSourceAndDestination = fopen("pathsEncryption.txt", "r");

    if (pathsSourceAndDestination == NULL)
    {
        perror("Error at opening paths for encryption file");
        printf("Filename : pathsEncryption.txt - you must have this file in order to read the paths of files for encryption\n");
        exit(EXIT_FAILURE);
    }

    fgets(pathEncryptionSource, 31, pathsSourceAndDestination);
    int temp = strlen(pathEncryptionSource);
    pathEncryptionSource[temp - 1] = '\0';

    fgets(pathEncryptionDestination, 31, pathsSourceAndDestination);
    temp = strlen(pathEncryptionDestination);
    pathEncryptionDestination[temp - 1] = '\0';

    fgets(pathEncryptionSecretKey, 31, pathsSourceAndDestination);
    temp = strlen(pathEncryptionSecretKey);
    pathEncryptionSecretKey[temp - 1] = '\0';

    fclose(pathsSourceAndDestination);

    encryption(pathEncryptionSource, pathEncryptionDestination, pathEncryptionSecretKey);

    char *pathDecryptionSource, *pathDecryptionDestination, *pathDecryptionSecretKey;

    pathDecryptionSource = (char *)malloc(sizeof(char) * 31);
    pathDecryptionDestination = (char *)malloc(sizeof(char) * 31);
    pathDecryptionSecretKey = (char *)malloc(sizeof(char) * 31);

    pathsSourceAndDestination = fopen("pathsDecryption.txt", "r");

    if (pathsSourceAndDestination == NULL)
    {
        perror("Error at opening paths for decryption file");
        printf("Filename : pathsDecryption.txt - you must have this file in order to read the paths of files for decryption\n");
        exit(EXIT_FAILURE);
    }

    fgets(pathDecryptionSource, 31, pathsSourceAndDestination);
    temp = strlen(pathDecryptionSource);
    pathDecryptionSource[temp - 1] = '\0';

    fgets(pathDecryptionDestination, 31, pathsSourceAndDestination);
    temp = strlen(pathDecryptionDestination);
    pathDecryptionDestination[temp - 1] = '\0';

    fgets(pathDecryptionSecretKey, 31, pathsSourceAndDestination);
    temp = strlen(pathDecryptionSecretKey);
    pathDecryptionSecretKey[temp - 1] = '\0';

    fclose(pathsSourceAndDestination);

    decryption(pathDecryptionSource, pathDecryptionDestination, pathDecryptionSecretKey);

    chiSqared(pathEncryptionSource);
    chiSqared(pathEncryptionDestination);

    free(pathEncryptionSource);
    free(pathEncryptionDestination);
    free(pathEncryptionSecretKey);
    free(pathDecryptionSource);
    free(pathDecryptionDestination);
    free(pathDecryptionSecretKey);

    unsigned char *imageForTemplateMatching, *copyImageForTemplateMatching;
    WINDOWS *detectionsReunion;
    unsigned int numberOfDetections, numberOfPatterns;
    unsigned int givenImageSize;
    double limit = 0.5;
    int i;

    char **patternCollection, *imagePathForTemplateMatching;

    patternCollection = (char **)malloc(sizeof(char *) * 11);
    for (i = 0; i < 10; i++)
    {
        patternCollection[i] = (char *)malloc(sizeof(char) * 12);
    }

    size_t imageAndPatternMaxSize = 31;
    imagePathForTemplateMatching = (char *)malloc(sizeof(char) * imageAndPatternMaxSize);

    FILE *imageAndPattern = fopen("pathsTemplateMatching.txt", "r");

    if (imageAndPattern == NULL)
    {
        perror("Error at opening paths for teplate matching file");
        printf("Filename : pathsTemplateMatching.txt - you must have this file in order to read the paths of files for template matching\n");
        exit(EXIT_FAILURE);
    }

    numberOfPatterns = 0;

    fgets(imagePathForTemplateMatching, imageAndPatternMaxSize, imageAndPattern);
    temp = strlen(imagePathForTemplateMatching);
    imagePathForTemplateMatching[temp - 1] = '\0';
    //input file has \n on last line to avoid not reading last string
    while (fgets(patternCollection[numberOfPatterns], imageAndPatternMaxSize, imageAndPattern) != NULL && (strcmp("\n", patternCollection[numberOfPatterns]) != 0))
    {
        temp = strlen(patternCollection[numberOfPatterns]);
        patternCollection[numberOfPatterns][temp - 1] = '\0';
        numberOfPatterns++;
    };

    fclose(imageAndPattern);

    FILE *templateInput = fopen(imagePathForTemplateMatching, "rb");

    if (templateInput == NULL)
    {
        perror("Error at reading image for template matching");
        printf("Filename : %s\n", imagePathForTemplateMatching);
        exit(EXIT_FAILURE);
    }

    fseek(templateInput, 0, SEEK_END);
    givenImageSize = ftell(templateInput);
    imageForTemplateMatching = (unsigned char *)malloc(sizeof(unsigned char) * (givenImageSize + 1));
    copyImageForTemplateMatching = (unsigned char *)malloc(sizeof(unsigned char) * (givenImageSize + 1));
    rewind(templateInput);

    fread(imageForTemplateMatching, givenImageSize, 1, templateInput);

    for (i = 0; i < givenImageSize; i++)
    {
        *(copyImageForTemplateMatching + i) = *(imageForTemplateMatching + i);
    }

    fclose(templateInput);

    detectionsReunion = (WINDOWS *)malloc(sizeof(struct point));
    numberOfDetections = 0;

    for (i = 0; i < numberOfPatterns; i++)
    {
        WINDOWS *temporaryWindows;
        temporaryWindows = (WINDOWS *)malloc(sizeof(struct point));
        int numberOfWindows, detectionsCounter;

        unsigned char *temporaryPattern;
        templateInput = fopen(patternCollection[i], "rb");
        if (templateInput == NULL)
        {
            perror("Error at reading pattern for template matching");
            printf("Filename : %s\n", patternCollection[i]);
            exit(EXIT_FAILURE);
        }
        
        int numberForColor = 0;
        if (strcmp(patternCollection[i], "cifra0.bmp") == 0)
        {
            numberForColor = 0;
        }
        else if (strcmp(patternCollection[i], "cifra1.bmp") == 0)
        {
            numberForColor = 1;
        }
        else if (strcmp(patternCollection[i], "cifra2.bmp") == 0)
        {
            numberForColor = 2;
        }
        else if (strcmp(patternCollection[i], "cifra3.bmp") == 0)
        {
            numberForColor = 3;
        }
        else if (strcmp(patternCollection[i], "cifra4.bmp") == 0)
        {
            numberForColor = 4;
        }
        else if (strcmp(patternCollection[i], "cifra5.bmp") == 0)
        {
            numberForColor = 5;
        }
        else if (strcmp(patternCollection[i], "cifra6.bmp") == 0)
        {
            numberForColor = 6;
        }
        else if (strcmp(patternCollection[i], "cifra7.bmp") == 0)
        {
            numberForColor = 7;
        }
        else if (strcmp(patternCollection[i], "cifra8.bmp") == 0)
        {
            numberForColor = 8;
        }
        else if (strcmp(patternCollection[i], "cifra9.bmp") == 0)
        {
            numberForColor = 9;
        }

        fseek(templateInput, 0, SEEK_END);
        givenImageSize = ftell(templateInput);
        temporaryPattern = (unsigned char *)malloc(sizeof(unsigned char) * givenImageSize);
        rewind(templateInput);

        fread(temporaryPattern, givenImageSize, 1, templateInput);

        fclose(templateInput);

        temporaryWindows = templateMatching(copyImageForTemplateMatching, temporaryPattern, limit, numberForColor, &numberOfWindows);

        for (detectionsCounter = 0; detectionsCounter < numberOfWindows; detectionsCounter++)
        {
            numberOfDetections++;
            detectionsReunion = (WINDOWS *)realloc(detectionsReunion, sizeof(struct point) * numberOfDetections);
            (detectionsReunion + numberOfDetections - 1)->x = (temporaryWindows + detectionsCounter)->x;
            (detectionsReunion + numberOfDetections - 1)->y = (temporaryWindows + detectionsCounter)->y;
            (detectionsReunion + numberOfDetections - 1)->digit = (temporaryWindows + detectionsCounter)->digit;
            (detectionsReunion + numberOfDetections - 1)->correlation = (temporaryWindows + detectionsCounter)->correlation;
        }

        free(temporaryPattern);
    }

    for (i = 0; i < 10; i++)
    {
        free(patternCollection[i]);
    }
    free(patternCollection);
    free(copyImageForTemplateMatching);

    nonMaximalElimination(&detectionsReunion, &numberOfDetections);
    conturingFinalDetections(&imageForTemplateMatching, detectionsReunion, numberOfDetections);

    givenImageSize = imageSize(imageForTemplateMatching);

    FILE *templateOutput = fopen(imagePathForTemplateMatching, "wb");
    fwrite(imageForTemplateMatching, givenImageSize, 1, templateOutput);
    fclose(templateOutput);

    free(imageForTemplateMatching);
    free(detectionsReunion);
    free(imagePathForTemplateMatching);
    return 0;
}