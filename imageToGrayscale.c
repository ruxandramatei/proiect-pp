void imageToGrayscale(unsigned char **imageWithHeader)
{
    unsigned int size = imageSize(*imageWithHeader);
    unsigned int i;
    for (i = 54; i < size; i += 3)
    {
        *(*imageWithHeader + i) = *(*imageWithHeader + i + 1) = (unsigned char)(*(*imageWithHeader + i + 2) =
                                                                                    0.299 * (*(*imageWithHeader + i + 2)) + 0.587 * (*(*imageWithHeader + i + 1)) +
                                                                                    0.114 * (*(*imageWithHeader + i)));
    }
}