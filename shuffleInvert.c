unsigned int *shuffleInvert(unsigned int *input, unsigned int size)
{
    unsigned int *shuffle;
    unsigned int shuffleCounter;

    shuffle = (unsigned int *)malloc(sizeof(unsigned int) * size);

    for (shuffleCounter = 0; shuffleCounter < size; shuffleCounter++)
    {
        *(shuffle + *(input + shuffleCounter)) = shuffleCounter;
    }
    free(input);
    return shuffle;
}