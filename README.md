# Procedural Programming Project

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is a school project written in C, which 
revolves around the idea of cryptography and pattern matching using as support
BMP images.

## Overview

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A transmitter(A) sends to a receiver(B) a BMP 
image which is encrypted using an encryption algorithm.In order to view the image, 
the receiver decrypts it.After that, he performs a handwritten digit recognition.
Finally, he sends back the resulting image 
to (A), image which is, again, encrypted.Person (A) decrypts the image and sees the solution,
the handwritting recognition.
