unsigned int *shuffleOperation(unsigned int *input, unsigned int size)
{
    unsigned int imageCounter, shuffleCounter, randomCounter;
    unsigned int *shuffled;

    shuffled = (unsigned int *)malloc(size * sizeof(unsigned int));

    for (shuffleCounter = 0; shuffleCounter < size; shuffleCounter++)
    {
        *(shuffled + shuffleCounter) = shuffleCounter;
    }

    for (imageCounter = size - 1, randomCounter = 1; imageCounter > 0; imageCounter--, randomCounter++)
    {
        unsigned int temp = (*(input + randomCounter)) % (imageCounter + 1);
        unsigned int aux = *(shuffled + temp);
        *(shuffled + temp) = *(shuffled + imageCounter);
        *(shuffled + imageCounter) = aux;
    }
    
    return shuffled;
}