unsigned int imageHeight(unsigned char *header)
{
    return *(header + 22) + (unsigned int)*(header + 23) * 256 + (unsigned int)*(header + 24) * 256 * 256 + (unsigned int)*(header + 25) * 256 * 256 * 256;
}