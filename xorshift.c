unsigned int *XORSHIFT32(unsigned int length, unsigned int startingValue)
{
    int counter;
    unsigned int *seed;

    seed = (unsigned int *)malloc(sizeof(unsigned int) * (length + 1));
    *(seed) = startingValue;

    for (counter = 1; counter < length; counter++)
    {
        *(seed + counter) = *(seed + counter - 1);
        *(seed + counter) = (*(seed + counter)) ^ (*(seed + counter) << 13);
        *(seed + counter) = (*(seed + counter)) ^ (*(seed + counter) >> 17);
        *(seed + counter) = (*(seed + counter)) ^ (*(seed + counter) << 5);
    }

    return seed;
}